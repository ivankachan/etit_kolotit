#include "main_test.h"

TEST(Testing, TwoPlusTwo) {
    EXPECT_EQ(summing(2,3), 5);
}

TEST(Testing, TenPlusTen) {
    EXPECT_EQ(summing(10,10), 20);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

