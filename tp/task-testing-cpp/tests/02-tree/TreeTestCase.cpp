//
// Created by akhtyamovpavel on 5/1/20.
//


#include "TreeTestCase.h"
#include "Tree.h"
#include <fstream>
TEST(BasicTests2, GetTreeTest1) {
  path pth = std::filesystem::temp_directory_path();
  ASSERT_THROW(GetTree("pth.string()", true), std::invalid_argument);

  FileNode answ;
  answ.name = std::filesystem::path(pth.string()).filename().string();
  answ.is_dir = true;
  for (auto& child: std::filesystem::directory_iterator(pth.string())) {
    if (std::filesystem::is_directory(child)) {
      answ.children.push_back(GetTree(std::filesystem::path(child).string(), true));
    }
  }
  ASSERT_EQ(GetTree(pth.string(), true).name, answ.name);
}

TEST(BasicTests2, GetTreeTest2) {
  path pth = std::filesystem::temp_directory_path() / "what.txt";
  std::filesystem::create_directory(std::filesystem::temp_directory_path());
  std::ofstream(pth.string());
  ASSERT_THROW(GetTree(pth.string(), true), std::invalid_argument);
}

TEST(BasicTests2, GetTreeTest3) {
  path pth = std::filesystem::temp_directory_path();
  FileNode answ;
  answ.name = std::filesystem::path(pth.string()).filename().string();
  answ.is_dir = true;
  for (auto& child: std::filesystem::directory_iterator(pth.string())) {
    if (std::filesystem::is_directory(child)){
      answ.children.push_back(
          GetTree(std::filesystem::path(child).string(), false)
      );
    } else {
      answ.children.push_back({
        std::filesystem::path(child).filename().string(), false, {}
      });
    }
  }
  ASSERT_EQ(GetTree(pth.string(), false).name, answ.name);
}

TEST(BasicTests2, FilterEmptyNodesTest) {
  path pth = std::filesystem::temp_directory_path() / "what.txt";
  std::filesystem::create_directory(std::filesystem::temp_directory_path());
  std::ofstream(pth.string());
  FileNode input;
  input.name = pth.string();
  input.is_dir = false;
  FilterEmptyNodes(input, ".");
  ASSERT_EQ(std::filesystem::exists(pth.string()), true);
}

TEST(BasicTests2, FilterEmptyNodesTest2) {
  path pth = std::filesystem::temp_directory_path() / "mydir";
  FileNode parent, empty_kid;
  empty_kid.name = pth.string();
  parent.is_dir = true;
  empty_kid.is_dir = true;
  parent.children = {empty_kid};
  FilterEmptyNodes(parent, ".");
  ASSERT_EQ(std::filesystem::exists("./empty_kid.name"), false);
  FileNode build;
  build.is_dir = true;
  EXPECT_ANY_THROW(FilterEmptyNodes(build, "."));
}

TEST(BasicTests2, OperatorTest) {
  FileNode first, second;
  first.name = "name.my";
  second.name = "name.my";
  first.is_dir = true;
  second.is_dir = true;
  ASSERT_TRUE(first == second);
}



