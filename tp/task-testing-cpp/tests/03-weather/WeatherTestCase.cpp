//
// Created by Pavel Akhtyamov on 02.05.2020.
//

#include "WeatherTestCase.h"
#include "WeatherMock.h"
#include <iostream>

TEST(Testik, Testicheck) {
  Weather weather;
  weather.SetApiKey("uOlglmW3zA6dFBV5AAaJyQ0enUcdwuTp");
  ASSERT_FALSE(weather.GetTemperature("London") == 2345678);
  ASSERT_FALSE(weather.GetTomorrowTemperature("London") == 2345678);
  ASSERT_FALSE(weather.GetTomorrowDiff("Moscow") == "2345678");
  ASSERT_FALSE(weather.GetDifferenceString("Minsk", "Moscow") == "ya pidaras");
}

