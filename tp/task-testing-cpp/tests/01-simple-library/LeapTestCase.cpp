//
// Created by akhtyamovpavel on 5/1/20.
//

#include "LeapTestCase.h"

#include <Functions.h>

TEST(BasicTests, IsLeapTestCase) {
  ASSERT_THROW(IsLeap(0), std::invalid_argument);
  ASSERT_EQ(IsLeap(2000), true);
  ASSERT_FALSE(IsLeap(2100));
  ASSERT_TRUE(IsLeap(2020));
  ASSERT_FALSE(IsLeap(2023));
}

TEST(BasicTests, GetMonthDaysTestCase) {
  ASSERT_EQ(GetMonthDays(2000, 2), 29);
  ASSERT_EQ(GetMonthDays(2001, 2), 28);
  EXPECT_ANY_THROW(GetMonthDays(2021, 0));
  EXPECT_ANY_THROW(GetMonthDays(2021, 13));
  ASSERT_EQ(GetMonthDays(1930, 2), 30);
  ASSERT_EQ(GetMonthDays(2000, 6), 30);
  ASSERT_EQ(GetMonthDays(2000, 7), 31);
}




