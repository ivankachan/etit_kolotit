#!/bin/bash

while [ -n "$1" ]
do
	case $1 in
		--input_folder) INPUT_FOLDER=$2;;
		--extension) EXTENSION=$2;;
		--backup_folder) BACKUP_FOLDER=$2;;
		--backup_archive_name) BACKUP_ARCHIVE_NAME=$2;;
	esac
	shift
	shift
done
mkdir $BACKUP_FOLDER
for FILES in $(find $INPUT_FOLDER -type f -name "*.$EXTENSION")
do
	cp --backup=numbered $FILES $BACKUP_FOLDER
done
tar -czpf $BACKUP_ARCHIVE_NAME $BACKUP_FOLDER

echo "done"
