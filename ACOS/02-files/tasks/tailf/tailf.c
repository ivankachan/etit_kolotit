#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdbool.h>

const size_t kBuff = 1024;

int main(int argc, char *argv[]) {
  if (argc != 2) {
    return 1;
  }
  const char *file = argv[1];
  int reader = open(file, O_RDONLY);
  if (reader == -1) {
    return -1;
  }
  char buff[kBuff];
  ssize_t readed;
  ssize_t checker;
  while ((readed = read(reader, buff, sizeof(buff))) > 0) {
    checker = write(1, buff, readed);
    if (checker < 0) {
      return -1;
    }
  }
  while (true) {
//    lseek(reader, 0, SEEK_SET);
    while ((readed = read(reader, buff, sizeof(buff))) > 0) {
      checker = write(1, buff, readed);
      if (checker < 0) {
        return -1;
      }
      fflush(stdout);
    }
//    usleep(100);
  }
  return 0;
}
