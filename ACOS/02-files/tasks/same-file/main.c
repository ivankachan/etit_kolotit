#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

bool is_same_file(const char* lhs_path, const char* rhs_path) {
  //struct lhs_path_info = lstat lhs_path;
  struct stat one;
  struct stat second;
  if (stat(lhs_path, &one) != 0 || stat(rhs_path, &second) != 0) {
    return false;
  }
    return (one.st_ino == second.st_ino);
}

int main(int argc, const char* argv[]) {
  if (argc != 3) {
    return -1;
  }
  const char* lhs_path = argv[1];
  const char* rhs_path = argv[2];
  if (is_same_file(lhs_path, rhs_path)) {
    printf("yes");
  } else {
    printf("no");
  }
    return 0;
}
