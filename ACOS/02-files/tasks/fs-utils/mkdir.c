#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>

ssize_t numpos(const char* path, const char a, size_t num) {
  size_t length = strlen(path);
  ssize_t counter = 0;
    for (ssize_t i = 0; i < length; ++i) {
      if (path[i] == a) {
        ++counter;
      }
      if (counter == num) {
        return i;
      }
      if ((i == length - 1) && counter != num) {
        return -1;
      }
    }
  return 0;
}
ssize_t numofchar(const char* path, const char a) {
  size_t length = strlen(path);
  ssize_t counter = 0;
  for (ssize_t i = 0; i < length; ++i) {
    if (path[i] == a) {
      ++counter;
    }
  }
  return counter;
}

int creator(const char* path, mode_t mode) {
  ssize_t number = numofchar(path, '/');
  for (ssize_t i = 1; i <= number; ++i) {
    ssize_t place = numpos(path, '/', i);
    if (place != -1) {
      if(place == 0) {
        continue;
      }
      char* copy = (char*)malloc(place + 1);
      if (copy == NULL) {
        continue;
      }
      strncpy(copy, path, place);
      copy[place] = '\0';
      DIR *dir = opendir(copy);
      if (dir != NULL) {
        closedir(dir);
        continue;
      }
      if (mkdir(copy, mode) == -1) {
        free(copy);
        return -1;
      }
      free(copy);
    }
  }
  return 0;
}
int create_directory(const char *path, mode_t mode, bool parents) {
  if (parents) {
    creator(path, mode);
  }
  if (parents == false) {
    if (mkdir(path, mode) != 0) {
      return -1;
    }
  }
    DIR *dir = opendir(path);
    if (dir != NULL) {
      closedir(dir);
      return 0;
    } else {
      if (mkdir(path, mode) == -1) {
        return -1;
      }
    }
  return 0;
}
int main(int argc,const char *argv[]) {
  if (argc < 2) {
    return -1;
  }
  const char* path = NULL;
  bool parent = false;
  mode_t mode = 0777;
  for (int i = 1; i <= argc - 1; ++i){
    if (strcmp(argv[i], "-p") == 0) {
      parent = true;
    }
    if (strcmp(argv[i], "--mode (-m)") == 0) {
      mode = atoi(argv[i+1]);
    }
    if (argv[i][0] != '-') {
      path = argv[i];
    }
  }
  if(create_directory(path, mode, parent) == -1) {
    return -1;
  }
  return 0;
}
