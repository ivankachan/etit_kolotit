#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>
#include <limits.h>
int remove_file(const char *path) {
  struct stat st;
  if (lstat(path, &st) == -1) {
    return -1;
  }
  if (S_ISDIR(st.st_mode)) {
    return -1;
  }
  if (unlink(path) == -1) {
    return -1;
  }
  return 0;
}
int remove_directory(const char *path) {
  struct stat st;
  if (lstat(path, &st) == -1) {
    return -1;
  }
  if (!S_ISDIR(st.st_mode)) {
    return -1;
  }
  DIR *dir = opendir(path);
  if (dir == NULL) {
    return -1;
  }
  struct dirent *entry;
  while ((entry = readdir(dir)) != NULL) {
    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
      char entry_path[PATH_MAX];
      snprintf(entry_path, sizeof(entry_path), "%s/%s", path, entry->d_name);
      struct stat st;
      if (lstat(entry_path, &st) == -1) {
        closedir(dir);
        return -1;
      }
      if (S_ISDIR(st.st_mode)) {
        if (remove_directory(entry_path) == -1) {
          closedir(dir);
          return -1;
        }
      } else {
        if (unlink(entry_path) == -1) {
          closedir(dir);
          return -1;
        }
      }
    }
  }
  closedir(dir);
  if (rmdir(path) == -1) {
    return -1;
  }
  return 0;
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    return -1;
  }
  bool recursive = false;
  const char *path = NULL;
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-r") == 0) {
      recursive = true;
    }
  }
  for (int i = 1; i < argc; ++i) {
    if (strcmp(argv[i], "-r") == 0) {
      continue;
    } else {
      path = argv[i];
      int result;
      struct stat st;
      if (lstat(path, &st) == -1) {
        return -1;
      }
      if (!S_ISDIR(st.st_mode)) {
        if (unlink(path) == -1) {
          return -1;
        }
        continue;
      }
      DIR *dir = opendir(path);
      if (dir == NULL) {
        return -1;
      }
      struct dirent *entry;
      int count = 0;
      while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
          count++;
        }
      }
      closedir(dir);
      if (count == 0) {
        if (rmdir(path) == -1) {
          return -1;
        }
      }
      if (recursive) {
        result = remove_directory(path);
        if (result == -1) {
          return -1;
        }
      } else {
        return -1;
      }
    }
  }
  return 0;
}