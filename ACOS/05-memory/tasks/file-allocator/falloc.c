#include "falloc.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void falloc_init(file_allocator_t* allocator, const char* filepath,
                 uint64_t allowed_page_count) {
  int exist = 1;
  int fd;
  off_t file_size = (allowed_page_count + 1) * PAGE_SIZE;
  if (access(filepath, F_OK) == 0) {
    fd = open(filepath, O_RDWR);
  } else {
    exist = 0;
    fd = open(filepath, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    if (fd == -1) {
      perror("Error opening/creating file");
      exit(EXIT_FAILURE);
    }
    lseek(fd, PAGE_SIZE * (allowed_page_count + 1), SEEK_SET);
    write(fd, "\0", 1);
    lseek(fd, 0, SEEK_SET);
  }
  allocator->fd = fd;
  allocator->allowed_page_count = allowed_page_count;
  if (ftruncate(allocator->fd, file_size) == -1) {
    perror("Error truncating file");
    close(allocator->fd);
    exit(EXIT_FAILURE);
  }
  void* preresult = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, allocator->fd, 0);
  allocator->base_addr = preresult + PAGE_SIZE;
  allocator->page_mask = (uint64_t*)preresult;
  if (exist == 0) {
    allocator->curr_page_count = 0;
    for (int i = 0; i < PAGE_MASK_SIZE; ++i) {
      (allocator->page_mask)[i] = 0;
    }
  }
}

void falloc_destroy(file_allocator_t* allocator) {

  munmap(allocator->base_addr - PAGE_SIZE, ((allocator->allowed_page_count + 1) * PAGE_SIZE));
  allocator->page_mask = NULL;
  allocator->base_addr = NULL;
  close(allocator->fd);

}

void* falloc_acquire_page(file_allocator_t* allocator) {
  if (allocator->curr_page_count >= allocator->allowed_page_count) {
    return NULL;
  }
  for (int i = 0; i < PAGE_MASK_SIZE; ++i) {
    uint64_t mask = allocator->page_mask[i];
    if (mask != UINT64_MAX) {
      int j = __builtin_ctzll(~mask);  // Находим индекс первого нулевого бита с использованием встроенной функции
      allocator->curr_page_count++;
      allocator->page_mask[i] |= (1ULL << j);  // Устанавливаем бит в маске
      return allocator->base_addr + PAGE_SIZE * (i * sizeof(uint64_t) * 8 + j);
    }
  }
  return NULL;
}

void falloc_release_page(file_allocator_t* allocator, void** addr) {
  uint64_t index = ((char*)*addr - (char*)allocator->base_addr) / PAGE_SIZE;
  *addr = NULL;
  if (index >= allocator->allowed_page_count) {
    fprintf(stderr, "Invalid page address\n");
    exit(EXIT_FAILURE);
  }
  allocator->page_mask[index / 64] &= ~(1ULL << (index % 64));
  allocator->curr_page_count--;
}