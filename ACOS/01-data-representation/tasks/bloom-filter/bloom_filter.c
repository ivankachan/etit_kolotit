#include "bloom_filter.h"
#include <string.h>
#include <stdlib.h>

uint64_t calc_hash(const char* str, uint64_t modulus, uint64_t seed) {
  uint64_t result = 0;
  uint64_t mult = seed;
  for (uint64_t i = 0; i < strlen(str); ++i) {
    result += str[i] * mult;
    result %= modulus;
    mult *= seed;
    mult %= modulus;
  }
  result %= modulus;
  return result;
}

void bloom_init(struct BloomFilter* bloom_filter, uint64_t set_size, hash_fn_t hash_fn, uint64_t hash_fn_count) {
  bloom_filter->hash_fn_count = hash_fn_count;
  bloom_filter->hash_fn = hash_fn;
  bloom_filter->set_size = set_size;
  bloom_filter->set = (uint64_t*) calloc((set_size)/ 64 + 1, sizeof(uint64_t));
}

void bloom_destroy(struct BloomFilter* bloom_filter) {
  free(bloom_filter->set);
  bloom_filter->set = NULL;
  bloom_filter->set_size = 0;
  bloom_filter->hash_fn_count = 0;
}

void bloom_insert(struct BloomFilter* bloom_filter, Key key) {
  for (uint64_t i = 0; i < bloom_filter->hash_fn_count; ++i) {
    uint64_t hash_res = calc_hash(key, bloom_filter->set_size, i);
    uint64_t pos_of_set = hash_res / 64;
    uint64_t pos_in_uint = hash_res % 64;
    (bloom_filter->set[pos_of_set]) |= (1ul << pos_in_uint);
  }
}

bool bloom_check(struct BloomFilter* bloom_filter, Key key) {
  for (uint64_t i = 0; i < bloom_filter->hash_fn_count; ++i) {
    uint64_t hash_res = calc_hash(key, bloom_filter->set_size, i);
    uint64_t pos_of_set = hash_res / 64;
    uint64_t pos_in_uint = hash_res % 64;
    if (!((bloom_filter->set[pos_of_set]) & (1ul << pos_in_uint))) {
      return false;
    }
  }
  return true;
}
