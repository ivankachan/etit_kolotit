#include "utf8_file.h"
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 4096

int utf8_write(utf8_file_t* f, const uint32_t* str, size_t count) {
  size_t bytesWritten = 0;
  uint8_t buf[4];
  for (size_t i = 0; i < count; i++) {
    if (str[i] < 0x80) {
      buf[0] = (uint8_t)str[i];
      if (write(f->fd, buf, 1) != 1) {
        return -1;
      }
      bytesWritten++;
    } else {
      size_t j = 0;
      uint32_t temp = str[i];
      while (temp > 0) {
        buf[j] = 0x80 | (temp & 0x3F);
        temp >>= 6;
        j++;
      }
      buf[j - 1] |= (~(0xFF >> j)) & 0xFF;
      for (ssize_t k = j - 1; k >= 0; k--) {
        if (write(f->fd, &buf[k], 1) != 1) {
          return -1;
        }
      }
      bytesWritten++;
    }
  }
  return bytesWritten;
}
int read_byte(utf8_file_t* f, uint8_t* byte) {
  ssize_t bytes_read = read(f->fd, byte, 1);
  if (bytes_read == 1) {
    return 1;
  } else if (bytes_read == 0) {
    return 0;
  } else {
    return -1;
  }
}

int utf8_read(utf8_file_t* f, uint32_t* res, size_t count) {
  size_t read_count = 0;
  while (read_count < count) {
    uint8_t current_byte = 0;
    int checker = read_byte(f, &current_byte);
    if (checker == 0) {
      return read_count;
    } else if (checker == -1) {
      return -1;
    }
    int bytes_to_read = 0;
    uint32_t unicode_char = 0;
    if ((current_byte & 0x80) == 0) {
      unicode_char = current_byte;
      bytes_to_read = 1;
    } else {
      uint8_t copy_of_first = current_byte;
      while ((copy_of_first & 0x80) != 0) {
        copy_of_first <<= 1;
        bytes_to_read++;
      }
      if (bytes_to_read >= 7) {
        return -1;
      }
      current_byte = current_byte & ((1 << (8 - bytes_to_read)) - 1);
      unicode_char = current_byte;
      for (int i = 1; i < bytes_to_read; i++) {
        if (read_byte(f, &current_byte) == -1) {
          return -1;
        }
        if ((current_byte & 0xC0) != 0x80) {
          return -1;
        }
        unicode_char = (unicode_char << 6) | (current_byte & 0x3F);
      }
    }
    res[read_count] = unicode_char;
    read_count++;
  }
  return read_count;
}



utf8_file_t * utf8_fromfd(int fd) {
  utf8_file_t * ptr = (utf8_file_t * ) malloc(sizeof(utf8_file_t));
  ptr -> fd = fd;
  return ptr;
}