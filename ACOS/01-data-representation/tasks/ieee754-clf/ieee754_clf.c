#include <stdint.h>
#include <string.h>
#include <math.h>
#include "ieee754_clf.h"
const size_t kSign = 63;
const size_t kExpon = 53;
const size_t kMantiss = 12;
const size_t kPow = 11;
union DoubleOrInt64 {
  double numb_double;
  uint64_t uint;
};
uint64_t Translater(double value) {
  union DoubleOrInt64 converter;
  converter.numb_double = value;
  uint64_t bits = converter.uint;
  return bits;
}
float_class_t classify(double x) {
  uint64_t sign, mantiss, exp;
  uint64_t bits = Translater(x);
  sign = bits >> kSign;
  exp = (bits << 1) >> kExpon;
  mantiss = (bits << kMantiss) >> kMantiss;
  if (exp != 0 && exp != (pow(2, kPow) - 1)) {
    if (sign == 1) {
      return MinusRegular;
    } else {
      return Regular;
    }
  }
  if (exp == 0 && mantiss == 0) {
    if (sign == 1) {
      return MinusZero;
    } else {
      return Zero;
    }
  }
  if (exp == 0) {
    if (sign == 1) {
      return MinusDenormal;
    } else {
      return Denormal;
    }
  }
  if (mantiss == 0 && exp == pow(2, 11) - 1) {
    if (sign == 1) {
      return MinusInf;
    } else {
      return Inf;
    }
  }

  if (mantiss != 0 && exp == pow(2, 11) - 1) {
    return NaN;
  }
  return Zero;
}