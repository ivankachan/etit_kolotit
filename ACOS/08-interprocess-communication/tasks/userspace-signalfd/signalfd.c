#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

int pipik[2];

void InstallHandler(int signum, void (*handler)(int)) {
  struct sigaction new_sigaction;
  new_sigaction.sa_handler = handler;
  if (sigaction(signum, &new_sigaction, NULL) == -1) {
    perror("Failed to install signal handler");
    exit(EXIT_FAILURE);
  }
}

void signal_handler(int signo) {
  int w = write(pipik[1], &signo, sizeof(int));
  if (w < 0) {
    perror("Write to pipe failed");
    exit(EXIT_FAILURE);
  }
}

int signalfd() {
  if (pipe(pipik) == -1) {
    perror("Failed to create pipe");
    return -1;
  }

  for (int sig_num = 1; sig_num < 32; ++sig_num) {
    if ((sig_num != SIGKILL) && (sig_num != SIGSTOP)) {
      InstallHandler(sig_num, signal_handler);
    }
  }

  return pipik[0];
}
