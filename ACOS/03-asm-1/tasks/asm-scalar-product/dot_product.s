    .intel_syntax noprefix

    .text
    .global dot_product

dot_product:
    xorps xmm0, xmm0
    mov eax, 0
loop_start:
    movss xmm1, [rsi + 4*rax]
    movss xmm2, [rdx + 4*rax]
    mulss xmm1, xmm2
    addss xmm0, xmm1
    add eax, 1
    cmp eax, edi
    jbe loop_start
loop_end:
    ret
