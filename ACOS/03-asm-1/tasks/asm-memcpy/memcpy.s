.intel_syntax noprefix

.text
.global my_memcpy

my_memcpy:
    push rbp
    mov rbp, rsp

    mov ecx, edx
    rep movsb
    mov rsp, rbp
    pop rbp
    ret
