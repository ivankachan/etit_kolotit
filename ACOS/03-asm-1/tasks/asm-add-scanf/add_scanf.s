  .intel_syntax noprefix

  .text
  .global add_scanf

add_scanf:
    push rbp
    mov rbp, rsp
    sub rsp, 16
    mov rdi, offset scanf_format_string
    lea rsi, [rbp - 16]
    lea rdx, [rbp - 8]
    call scanf
    mov rax, qword ptr [rbp - 8]
    add rax, qword ptr [rbp - 16]
    mov rsp, rbp
    pop rbp
    ret

  .section .rodata
scanf_format_string:
  .string "%lld %lld"