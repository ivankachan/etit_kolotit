#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lca.h"

int getppidbypid(int pid) {
  char str[256];
  FILE* file;
  memset(str, 0, sizeof(str));
  sprintf(str, "/proc/%d/stat", pid);
  file = fopen(str, "r");
  if (file == NULL) {
    perror("Error opening file");
    return -1;
  }
  int ppid;
  if (fscanf(file, "%*d %*s %*c %d", &ppid) != 1) {
    fclose(file);
    return -1;
  }
  fclose(file);
  return ppid;
}

pid_t find_lca(pid_t x, pid_t y) {
    if (x == y) {
    return x;
  }
  if (getppidbypid(x) == y) {
    return y;
  }
  if (getppidbypid(y) == x) {
    return x;
  }
  if (getppidbypid(x) == getppidbypid(y)) {
    return getppidbypid(x);
  }
  int x_ancesters[MAX_TREE_DEPTH];
  int y_ancesters[MAX_TREE_DEPTH];
  memset(x_ancesters, 0, MAX_TREE_DEPTH);
  memset(y_ancesters, 0, MAX_TREE_DEPTH);
  for (int i = 0; i < MAX_TREE_DEPTH; ++i) {
    x_ancesters[i] = getppidbypid(x);
    x = getppidbypid(x);
    if (x == 1) {
      break;
    }
  }
  for (int i = 0; i < MAX_TREE_DEPTH; ++i) {
    y_ancesters[i] = getppidbypid(y);
    y = getppidbypid(y);
    if (y == 1) {
      break;
    }
  }
  int lca;
  for (int first = 0; first < MAX_TREE_DEPTH; ++first) {
    int value_first = x_ancesters[first];
    for (int second = 0; second < MAX_TREE_DEPTH; ++second) {
      if (y_ancesters[second] == 0) {
        break;
      }
      if (value_first == y_ancesters[second]) {
        return value_first;
      }
    }
  }
  return 0;
}
