.text
.global add_scanf

.macro push Xn
  sub sp, sp, 16
  str \Xn, [sp, 0]
.endm

.macro pop Xn
  ldr \Xn, [sp, 0]
  add sp, sp, 16
.endm

add_scanf:
  push x29
  push x30
  mov x29, sp
  sub sp, sp, 16
  ldr x0, =scanf_format_string
  add x1, x29, 16
  add x2, x29, 8
  bl scanf
  ldr x3, [x29, 16]
  ldr x4, [x29, 8]
  add x0, x3, x4
  mov sp, x29
  pop x30
  pop x29
  ret

.section .rodata

scanf_format_string:
  .string "%lld %lld"
