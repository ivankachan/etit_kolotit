.text
  .global longest_inc_subseq

longest_inc_subseq:
  mov x12, #0               //х12 = 0
  mov x13, #1               //х13 = 1
  mov x14, #-1              // х14 = -1
  cmp x2, x12               // Проверка размера на ноль
  bgt check_non_zero
  mov x0, x12
  ret

check_non_zero:
  mov x3, x12
  mov x11, x13
start_of_loop:              //условие цикла
  cmp x3, x2
  bge end_of_loop
  str  x11, [x1, x3, LSL #3]
  mov x4, x14
  innerloop:
    add x4, x4, x13
    cmp x4, x3
    beq innerloopend
    ldr x5, [x0, x3, LSL #3]
    ldr x6, [x0, x4, LSL #3]
    cmp x6, x5
    bge innerloop
    ldr x9, [x1, x3, LSL #3]
    ldr x10, [x1, x4, LSL #3]
    add x10, x10, x13
    cmp x9, x10
    bge innerloop
    str x10, [x1, x3, LSL #3]
    b innerloop
  innerloopend:
  add x3, x3, x13
  b start_of_loop

end_of_loop:
  ldr x15, [x1]
  mov x3, x14
finding_maximum:                // находим ответ как максимум по дп
  add x3, x3, x13
  cmp x3, x2
  beq end_finding_maximum
  ldr x9, [x1, x3, LSL #3]
  cmp x15, x9
  bhi finding_maximum
  mov x15, x9
  b finding_maximum
end_finding_maximum:
  mov x0, x15                  // выводим ответ как х0
  ret
